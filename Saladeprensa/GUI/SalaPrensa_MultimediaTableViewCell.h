//
//  SalaPrensa_MultimediaTableViewCell.h
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 27/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalaPrensa_MultimediaTableViewController.h"
@interface SalaPrensa_MultimediaTableViewCell : UITableViewCell


@property (nonatomic,retain) NSArray* objects;
@property (nonatomic) SalaPrensa_MultimediaTableViewController* parentViewController;

-(void)loadCell;


@end
