//
//  SalaPrensa_NewsTableViewCell.h
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 20/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalaPrensa_NewsTableViewCell : UITableViewCell


@property (nonatomic,retain) NSMutableDictionary* object;


-(void)loadCell;

@end
