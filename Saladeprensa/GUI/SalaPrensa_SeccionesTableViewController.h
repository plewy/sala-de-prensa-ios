//
//  SalaPrensa_SeccionesTableViewController.h
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 19/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalaPrensa_SeccionesTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


@end
