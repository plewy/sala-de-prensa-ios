//
//  SalaPrensa_NewsTableViewController.h
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 19/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SalaPrensa_NewsTableViewControllerDelegate
- (void)PushNewsWithObjectArray:(NSArray*)objectArray andIndex:(int)index;
@end

@interface SalaPrensa_NewsTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    
    NSString* urlString;
     
    id <SalaPrensa_NewsTableViewControllerDelegate> delegate;
}

@property (nonatomic,retain) NSArray* objectsArray;
@property (readwrite,retain) NSString* urlString;


    //IB
@property (retain, nonatomic) IBOutlet UITableView *feedTable;
@property (retain, nonatomic) IBOutlet UITextField *txtSearch;
@property (retain, nonatomic) IBOutlet UILabel *lblFecha;

@property (nonatomic, assign) id <SalaPrensa_NewsTableViewControllerDelegate> delegate;



- (IBAction)reloadButtonPressed:(id)sender;

@end
