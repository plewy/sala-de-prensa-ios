//
//  SalaPrensa_DetailNewsViewController.h
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 20/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalaPrensa_DetailNewsViewController : UIViewController <UIWebViewDelegate>
{
    int index;
}

@property (retain, nonatomic) IBOutlet UILabel *lblFecha;
@property (retain, nonatomic) IBOutlet UILabel *lblCounter;
@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (retain, nonatomic) IBOutlet UIButton *nextButton;
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain,nonatomic) NSArray* objectArray;
@property int index;

@property (retain, nonatomic) IBOutlet UIButton *reduceButton;
@property (retain, nonatomic) IBOutlet UIButton *augmentButton;


- (IBAction)volver:(id)sender;
- (IBAction)newsBack:(id)sender;
- (IBAction)newsNext:(id)sender;
- (IBAction)share:(id)sender;
- (IBAction)reduce:(id)sender;
- (IBAction)augment:(id)sender;
- (IBAction)fav:(id)sender;


@end
