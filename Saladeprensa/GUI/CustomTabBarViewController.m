//
//  CustomTabBarViewController.m
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 19/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import "CustomTabBarViewController.h"
#import "SalaPrensa_ConfigViewController.h"
#import "SalaPrensa_FavNewsTableViewController.h"
#import "SalaPrensa_MultimediaTableViewController.h"
#import "SalaPrensa_NewsTableViewController.h"
#import "SalaPrensa_SeccionesTableViewController.h"
#import "SalaPrensa_DetailNewsViewController.h"



@interface CustomTabBarViewController ()

@end

@implementation CustomTabBarViewController
@synthesize newsButton;
@synthesize categoriesButton;
@synthesize multimediaButton;
@synthesize favButton;
@synthesize configButton;

@synthesize configView = _configView;
@synthesize favView = _favView;
@synthesize mmView = _mmView;
@synthesize homeView = _homeView;
@synthesize sectionsView = _sectionsView;



- (void)viewDidLoad
{
    
    [newsButton setEnabled:NO];
    
    if(self.homeView == nil)
        self.homeView = [[SalaPrensa_NewsTableViewController alloc] initWithNibName:@"SalaPrensa_NewsTableViewController" bundle:nil];
  
    self.homeView.delegate = self;
    [self.view addSubview:self.homeView.view];

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    
    
    [self setNewsButton:nil];
    [self setMultimediaButton:nil];
    [self setFavButton:nil];
    [self setConfigButton:nil];
    [self setCategoriesButton:nil];
    
    [self setConfigView:nil];
    [self setFavView:nil];
    [self setMmView:nil];
    [self setHomeView:nil];
    [self setSectionsView:nil];
    
    
    [super viewDidUnload];
        // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)tabBarPressed:(UIButton *)sender {
    
    if(sender.enabled){
    
    [newsButton setEnabled:YES];
    [multimediaButton setEnabled:YES];
    [categoriesButton setEnabled:YES];
    [favButton setEnabled:YES];
    [configButton setEnabled:YES];
    sender.enabled = NO;
        
        
        for(int i = 0; i < [self.view.subviews count]; i++){
        
            UIView* view = [self.view.subviews objectAtIndex:i];
            
            if ([view isEqual:self.homeView.view])
            {   self.homeView.delegate = nil;
                [self.homeView.view removeFromSuperview];
                i--;
            }
                
            if ([view isEqual:self.sectionsView.view])
            {   [self.sectionsView.view removeFromSuperview];
                i--;

            }
            
            if ([view isEqual:self.mmView.view])
            {
                [self.mmView.view removeFromSuperview];
                i--;

            }
            if ([view isEqual:self.favView.view])
            {
                [self.favView.view removeFromSuperview];
                i--;

            }
            if ([view isEqual:self.configView.view])
            {
                [self.configView.view removeFromSuperview];
                i--;
            }
        }
    
        
        if([sender isEqual:newsButton])
        {
            
            if(self.homeView == nil)
                self.homeView = [[SalaPrensa_NewsTableViewController alloc] initWithNibName:@"SalaPrensa_NewsTableViewController" bundle:nil];
            
            self.homeView.delegate = self;
            [self.view addSubview:self.homeView.view];
            
            
        }else if([sender isEqual:multimediaButton])
        {
            if(self.mmView == nil)
                self.mmView = [[SalaPrensa_MultimediaTableViewController alloc] initWithNibName:@"SalaPrensa_MultimediaTableViewController" bundle:nil];
            self.mmView.delegate = self;
             [self.view addSubview:self.mmView.view];
            
        }else if([sender isEqual:categoriesButton])
        {
            if(self.sectionsView == nil)
                self.sectionsView = [[SalaPrensa_SeccionesTableViewController alloc] initWithNibName:@"SalaPrensa_SeccionesTableViewController" bundle:nil];
            
      
            [self.view addSubview:self.sectionsView.view];
            
        }else if([sender isEqual:favButton])
        {
            if(self.favView == nil)
                self.favView = [[SalaPrensa_FavNewsTableViewController alloc] initWithNibName:@"SalaPrensa_FavNewsTableViewController" bundle:nil];
       
            [self.view addSubview:self.favView.view];
            
        }else if([sender isEqual:configButton])
        {
            if(self.configView == nil)
                self.configView = [[SalaPrensa_ConfigViewController alloc] initWithNibName:@"SalaPrensa_ConfigViewController" bundle:nil];
            
            [self.view addSubview:self.configView.view];
        }
        
    }
}
- (void)dealloc {
    [newsButton release];
    [multimediaButton release];
    [favButton release];
    [configButton release];
    [categoriesButton release];
    
    [_configView release];
    [_favView release];
    [_mmView release];
    [_homeView release];
    [_sectionsView release];
    
    [super dealloc];
}
- (IBAction)categoriesButton:(id)sender {
}


    // News Deleate Methods
- (void)PushNewsWithObjectArray:(NSArray*)objectArray andIndex:(int)index;
{
    SalaPrensa_DetailNewsViewController* detailViewController = [[SalaPrensa_DetailNewsViewController alloc] initWithNibName:@"SalaPrensa_DetailNewsViewController" bundle:nil];
    
    detailViewController.objectArray = objectArray;
    detailViewController.index = index;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}


-(void)PushMultimediaWithObjectArray:(NSArray *)objectArray andIndex:(int)index{
    
    SalaPrensa_DetailNewsViewController* detailViewController = [[SalaPrensa_DetailNewsViewController alloc] initWithNibName:@"SalaPrensa_DetailNewsViewController" bundle:nil];
    
    detailViewController.objectArray = objectArray;
    detailViewController.index = index;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}
@end
