//
//  SalaPrensa_NewsTableViewController.m
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 19/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import "SalaPrensa_NewsTableViewController.h"
#import "SalaPrensa_NewsTableViewCell.h"
#import "helpers.h"
#import "MBProgressHUD.h"

@interface SalaPrensa_NewsTableViewController ()

@end



@implementation SalaPrensa_NewsTableViewController
@synthesize objectsArray = _objectsArray;
@synthesize urlString;
@synthesize feedTable;
@synthesize txtSearch;
@synthesize lblFecha;
@synthesize delegate;

MBProgressHUD* HUD;



- (void)viewDidLoad
{
    self.tabBarItem.image = [UIImage imageNamed:@"footer-ultimahora-normal-en-portrait"];
    urlString = @"http://sala-prensa.com/telefonica/contents/rss/3-noticias.xml/zone:1/lang:es";
    HUD = nil;
    
    [self performSelectorInBackground:@selector(loadFeeds) withObject:nil];
    
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setFeedTable:nil];
    [self setTxtSearch:nil];
    [self setLblFecha:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.objectsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SalaPrensa_NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if(cell == nil)
        cell = [[SalaPrensa_NewsTableViewCell alloc] init];
    
    cell.object = [self.objectsArray objectAtIndex:indexPath.row];
    [cell loadCell];

    return cell;
}




#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [delegate PushNewsWithObjectArray:self.objectsArray andIndex:indexPath.row];
 
}




    //Feed Reading


-(void)loadFeeds{
    
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    if(HUD == nil){  

    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.labelText = @"Cargando...";
        [self.view addSubview:HUD];
    
    }
    [HUD show:YES];

    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd 'de' MMMM"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    lblFecha.text = dateString;
    
    if(self.objectsArray != nil)
        [self.objectsArray release];
    
    self.objectsArray = [Helpers retrieveXMLfromURL:urlString:@"item":@"home" ];
    
    [self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];
    
    [pool release];
}

-(void)reloadTable{
    feedTable.delegate = self;
    feedTable.dataSource = self;
    [feedTable reloadData];
    [HUD hide:YES];
}

- (void)dealloc {
    [feedTable release];
    [txtSearch release];
    [lblFecha release];
    [super dealloc];
}
- (IBAction)reloadButtonPressed:(id)sender 
{
 
    [self performSelectorInBackground:@selector(loadFeeds) withObject:nil];
}
@end
