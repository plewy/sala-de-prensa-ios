//
//  SalaPrensa_NewsTableViewCell.m
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 20/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import "SalaPrensa_NewsTableViewCell.h"
#import "helpers.h"

@implementation SalaPrensa_NewsTableViewCell
@synthesize object;

UILabel* titleLabel;
UIImageView* imgView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)loadCell{
    
    self.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noticias-articlebg-portrait.png"]];
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    

    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(12,12, 50, 50)];
    imgView.contentScaleFactor = UIViewContentModeScaleAspectFit;
    imgView.image = [UIImage imageNamed:@"imagesize-potratit"];
    [self addSubview:imgView];
    
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(72, 12, 200, 50)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor colorWithRed:59.0/255.0 green:75.0/255.0 blue:81.0/255.0 alpha:1.0];
    titleLabel.font = [UIFont fontWithName:@"ArialMT" size:11];
    titleLabel.text = [object objectForKey:@"title"];
    titleLabel.numberOfLines = 3;
    
    [self addSubview:titleLabel];
    
    [self performSelectorInBackground:@selector(imgThread) withObject:nil];
    
}



-(void)imgThread{
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    
    NSString* imgURL = [[object objectForKey:@"media"]objectAtIndex:0];
    
    if(imgURL != nil && ![imgURL isEqualToString:@""]){
        
        [Helpers CreateDirectory:@"imgs"];
        
        UIActivityIndicatorView* indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] autorelease];
        [indicator startAnimating];
      
        indicator.frame = CGRectMake((imgView.frame.size.width / 2) - (indicator.frame.size.width / 2), (imgView.frame.size.height / 2) - (indicator.frame.size.height / 2), indicator.frame.size.width, indicator.frame.size.height);
        [imgView addSubview:indicator];

        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *directory = [[paths objectAtIndex:0]stringByAppendingPathComponent:@"imgs"];
        
        NSString* imgName = [imgURL lastPathComponent];
        
        NSData* imgData = nil;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[directory stringByAppendingPathComponent:imgName] isDirectory:nil])
            imgData = [NSData dataWithContentsOfFile:[directory stringByAppendingPathComponent:imgName]];
        else if([Helpers NetworkIsReacheable]) 
        { 
            imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgURL]];
            [imgData writeToFile:[directory stringByAppendingPathComponent:imgName] atomically:YES];
        }
        
        if(imgData != nil)
            [imgView  setImage:[UIImage imageWithData:imgData]];

        
    [indicator removeFromSuperview];
        
    }
    else {
        titleLabel.frame =  CGRectMake(12, 12, 250, 50);
        
    }
    [pool release];
}

-(void)dealloc{
    [titleLabel release];

    [super dealloc];
}



@end
