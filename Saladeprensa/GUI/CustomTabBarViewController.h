//
//  CustomTabBarViewController.h
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 19/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SalaPrensa_ConfigViewController.h"
#import "SalaPrensa_FavNewsTableViewController.h"
#import "SalaPrensa_MultimediaTableViewController.h"
#import "SalaPrensa_NewsTableViewController.h"
#import "SalaPrensa_SeccionesTableViewController.h"





@interface CustomTabBarViewController : UIViewController<SalaPrensa_NewsTableViewControllerDelegate,SalaPrensa_MultimediaTableViewControllerDelegate>{
 
}
@property (retain, nonatomic) IBOutlet UIButton *newsButton;
@property (retain, nonatomic) IBOutlet UIButton *categoriesButton;
@property (retain, nonatomic) IBOutlet UIButton *multimediaButton;
@property (retain, nonatomic) IBOutlet UIButton *favButton;
@property (retain, nonatomic) IBOutlet UIButton *configButton;





    //Views

@property (nonatomic,strong) SalaPrensa_ConfigViewController* configView;
@property (nonatomic,strong) SalaPrensa_FavNewsTableViewController* favView;
@property (nonatomic,strong) SalaPrensa_MultimediaTableViewController* mmView;
@property (nonatomic,strong) SalaPrensa_NewsTableViewController* homeView;
@property (nonatomic,strong) SalaPrensa_SeccionesTableViewController* sectionsView;



- (IBAction)tabBarPressed:(UIButton *)sender;

@end


