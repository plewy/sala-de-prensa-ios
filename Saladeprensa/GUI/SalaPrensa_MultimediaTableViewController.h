//
//  SalaPrensa_MultimediaTableViewController.h
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 19/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SalaPrensa_MultimediaTableViewControllerDelegate
- (void)PushMultimediaWithObjectArray:(NSArray*)objectArray andIndex:(int)index;
@end

@interface SalaPrensa_MultimediaTableViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>{

    NSString* urlVideoString;
    NSString* urlPhotoString;
    
    id <SalaPrensa_MultimediaTableViewControllerDelegate> delegate;


}
@property (nonatomic,retain) NSArray* objectsArray;
@property (readwrite,retain) NSString* urlVideoString;
@property (readwrite,retain) NSString* urlPhotoString;
    //IB
@property (retain, nonatomic) IBOutlet UITableView *feedTable;
@property (retain, nonatomic) IBOutlet UIButton *videoButton;
@property (retain, nonatomic) IBOutlet UIButton *photoButton;
@property (nonatomic, assign) id <SalaPrensa_MultimediaTableViewControllerDelegate> delegate;

- (IBAction)photoSelected:(id)sender;
- (IBAction)videoSelected:(id)sender;


- (void)itemSelectedWithObject:(NSDictionary*)obj;

@end
