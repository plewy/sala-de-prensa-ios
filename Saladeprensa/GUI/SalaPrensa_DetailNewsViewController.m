//
//  SalaPrensa_DetailNewsViewController.m
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 20/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import "SalaPrensa_DetailNewsViewController.h"
#import "SHK.h"

@interface SalaPrensa_DetailNewsViewController ()
-(void)customizeView;
-(void)loadContentWithIndex:(int)i;

@end

int titleSize = 13;
int bodySize = 11;

@implementation SalaPrensa_DetailNewsViewController
@synthesize lblFecha;
@synthesize lblCounter;
@synthesize backButton;
@synthesize nextButton;
@synthesize webView;
@synthesize objectArray = _objectArray;
@synthesize index;
@synthesize reduceButton;
@synthesize augmentButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self customizeView];
    // Do any additional setup after loading the view from its nib.
}


-(void)customizeView{
   
    lblCounter.text = [NSString stringWithFormat:@"%d de %d", index + 1, [self.objectArray count]];
    
    if(index < 1)
        backButton.enabled = NO;
    
    if(index >= [self.objectArray count] -1)
        nextButton.enabled = NO;
    
    [self loadContentWithIndex:index];
}


-(void)loadContentWithIndex:(int)i{
	
	NSString* templatePath = [[NSBundle mainBundle] pathForResource:@"template" ofType:@"html"];
	
	NSString* templateHTML = [[NSString alloc] initWithContentsOfFile:templatePath encoding:NSUTF8StringEncoding error:nil];
	
    templateHTML = [templateHTML stringByReplacingOccurrencesOfString:@"[TITLE]" withString:[[self.objectArray objectAtIndex:i] objectForKey:@"title"]];

	templateHTML = [templateHTML stringByReplacingOccurrencesOfString:@"[DATA]" withString:[[[self.objectArray objectAtIndex:i] objectForKey:@"description"] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
	
    
    NSString* imgURL = [[[self.objectArray objectAtIndex:i] objectForKey:@"media"]objectAtIndex:0];
    
    
    if(imgURL != nil && ![imgURL isEqualToString:@""])
    {
		
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *directory = [[paths objectAtIndex:0]stringByAppendingPathComponent:@"imgs"];
        
        
        NSString *baseImageURL = [[NSURL fileURLWithPath:[directory
 stringByAppendingPathComponent:[imgURL lastPathComponent]]] absoluteString];
        
        NSString * tempimg = @"<img src=\"[IMAGE]\" width=\"100%\"><br /><br />";
		tempimg = [tempimg stringByReplacingOccurrencesOfString:@"[IMAGE]" withString:baseImageURL];
		templateHTML =[templateHTML stringByReplacingOccurrencesOfString:@"[IMAGE]" withString:tempimg];
       
        
        
        UIImage*imgdata = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:baseImageURL]]];
        

        int heightA = imgdata.size.height;
        float ratio = imgdata.size.height / imgdata.size.width;
        int height = (320* ratio) - 20;
        
        	templateHTML =[templateHTML stringByReplacingOccurrencesOfString:@"[TOP]" withString:[NSString stringWithFormat:@"%d",height]];
        
        
        
	}
	else {
		templateHTML =[templateHTML stringByReplacingOccurrencesOfString:@"[IMAGE]" withString:@""];
	}
    
    
	webView.delegate = self;
	
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    
	[webView loadHTMLString:templateHTML baseURL:baseURL];
}


- (void)viewDidUnload
{
    [self setLblFecha:nil];
    [self setLblCounter:nil];
    [self setWebView:nil];
    [self setBackButton:nil];
    [self setNextButton:nil];
    [self setReduceButton:nil];
    [self setAugmentButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (IBAction)volver:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)newsBack:(id)sender {
    
    index--;
    
    if(index < [self.objectArray count])
        nextButton.enabled = YES;
    
    
    if(index <= 0){
        index = 0;
        backButton.enabled = NO;
    }
    else 
        backButton.enabled = YES;

    
    lblCounter.text = [NSString stringWithFormat:@"%d de %d", index + 1, [self.objectArray count]];
    [self loadContentWithIndex:index];
    
    
}

- (IBAction)newsNext:(id)sender {

    index ++;
     
    if(index > 0)
        backButton.enabled = YES;
    
   
    if(index < [self.objectArray count])
       nextButton.enabled = YES;
    else{
        index = [self.objectArray count] - 1;
        nextButton.enabled = NO;
    }
    
    
    lblCounter.text = [NSString stringWithFormat:@"%d de %d", index + 1, [self.objectArray count]];
    [self loadContentWithIndex:index];
    
}

- (IBAction)share:(id)sender {
    
    
        // Create the item to share (in this example, a url)
	NSURL *url = [NSURL URLWithString:[[self.objectArray objectAtIndex:index] objectForKey:@"link"]];
	SHKItem *item = [SHKItem URL:url title:[[self.objectArray objectAtIndex:index] objectForKey:@"title"]];
    
        // Get the ShareKit action sheet
	SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
    
        // Display the action sheet
    [SHK setRootViewController:self];
	[actionSheet showInView:self.view];
}

- (IBAction)reduce:(id)sender {
    
    
    if(titleSize < 18)
        augmentButton.enabled = YES;
    
    
    if(titleSize < 10)
    {
        titleSize = 10;
        bodySize = 8;
        reduceButton.enabled = NO;
    }else {
        reduceButton.enabled = YES;
    }
	
    
    titleSize --;
	bodySize --;
    
    
	NSString * tituloAument = [@"document.getElementById('title').style.fontSize ='[tamano]px';" stringByReplacingOccurrencesOfString:@"[tamano]" withString:[NSString stringWithFormat:@"%d", titleSize]];
	
	NSString* finalScript = tituloAument;
	
	
	if([[[self.objectArray objectAtIndex:index] objectForKey:@"description"] length] > 0)
	{
		NSString*textoAument = [@"document.getElementById('body').style.fontSize ='[tamano]px';" stringByReplacingOccurrencesOfString:@"[tamano]" withString:[NSString stringWithFormat:@"%d", bodySize]];
        
		finalScript = [finalScript stringByAppendingString:textoAument];
	}
	
	[webView stringByEvaluatingJavaScriptFromString:finalScript];

}




- (IBAction)augment:(id)sender {
    
    if(titleSize >= 10)
        reduceButton.enabled = YES;
        
    
    if(titleSize > 18)
    {
        titleSize = 18;
        bodySize = 16;
        augmentButton.enabled = NO;
    }
	else {
        augmentButton.enabled = YES;
    }
    
    titleSize ++;
	bodySize ++;
    
    
	
	NSString * tituloAument = [@"document.getElementById('title').style.fontSize ='[tamano]px';" stringByReplacingOccurrencesOfString:@"[tamano]" withString:[NSString stringWithFormat:@"%d", titleSize]];
	
	NSString* finalScript = tituloAument;
	
	
	if([[[self.objectArray objectAtIndex:index] objectForKey:@"description"] length] > 0)
	{
		NSString*textoAument = [@"document.getElementById('body').style.fontSize ='[tamano]px';" stringByReplacingOccurrencesOfString:@"[tamano]" withString:[NSString stringWithFormat:@"%d", bodySize]];
        
		finalScript = [finalScript stringByAppendingString:textoAument];
	}
	
	[webView stringByEvaluatingJavaScriptFromString:finalScript];
}

- (IBAction)fav:(id)sender {
}


- (void)dealloc {
    [lblFecha release];
    [lblCounter release];
    [webView release];
    [backButton release];
    [nextButton release];
    [reduceButton release];
    [augmentButton release];
    [super dealloc];
}


#pragma mark -
#pragma mark UIWebViewDelegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
}



- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}

@end
