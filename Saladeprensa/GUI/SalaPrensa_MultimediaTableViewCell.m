//
//  SalaPrensa_MultimediaTableViewCell.m
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 27/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import "SalaPrensa_MultimediaTableViewCell.h"
#import "helpers.h"
#define gridSeparator 13.5

@implementation SalaPrensa_MultimediaTableViewCell
@synthesize objects;
@synthesize parentViewController;

float gridObjectWidth = 101;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)loadCell{
    
    int i = 0;
    
    for (int c = 0; c < [self.subviews count]; c++) {
        UIView* view = [self.subviews objectAtIndex:0];
        [view removeFromSuperview];
        c--;
    }
    
    while(i < [objects count])
    {
        [self createGridObjectWithObject:[objects objectAtIndex:i] andNumber:i];
        i++;
    }
    
  }
    
-(void)createGridObjectWithObject: (NSDictionary*)object andNumber:(NSInteger)number {
    
    
    UIView* containerView = [[[UIView alloc] initWithFrame:CGRectMake(gridObjectWidth * number, 0, 93, 110)] autorelease];
    
    UIImageView* backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fotosvideosborder-potrait.png"]] autorelease];
    backgroundView.frame = CGRectMake(gridSeparator - 1.5, 8.5, 96, 74);
    [containerView addSubview:backgroundView];
    
    
    UIButton* imgButton = [[[UIButton alloc] initWithFrame:CGRectMake(gridSeparator, 10, 93, 70)] autorelease];
    
    imgButton.tag = number;
    [imgButton setImage:[UIImage imageNamed:@"imagesize-potratit.png"] forState:UIControlStateNormal];
    
    [imgButton addTarget:self action:@selector(gridObjectSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    [self performSelectorInBackground:@selector(loadImageInButton:) withObject:imgButton];
    
    [containerView addSubview:imgButton];
    
    
    UILabel* titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(gridSeparator, 85, 93, 20)] autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor colorWithRed:59.0/255.0 green:75.0/255.0 blue:81.0/255.0 alpha:1.0];
    titleLabel.font = [UIFont fontWithName:@"ArialMT" size:9];
    titleLabel.text = [object objectForKey:@"title"];
    titleLabel.numberOfLines = 2;

    [containerView addSubview:titleLabel];    
    
    [self addSubview:containerView];
}

-(void)loadImageInButton:(UIButton*)button{
    
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    [Helpers CreateDirectory:@"imgs"];
    
    UIActivityIndicatorView* indicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] autorelease];
    [indicator startAnimating];
    indicator.frame = CGRectMake((button.frame.size.width / 2) - (indicator.frame.size.width / 2), (button.frame.size.height / 2) - (indicator.frame.size.height / 2), indicator.frame.size.width, indicator.frame.size.height);
    [button addSubview:indicator];
    
    
    NSString* imgURL = [[[objects objectAtIndex:button.tag] objectForKey:@"media"]objectAtIndex:0];
    
        
    
    if(imgURL != nil && ![imgURL isEqualToString:@""])
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *directory = [[paths objectAtIndex:0]stringByAppendingPathComponent:@"imgs"];
        
        NSString* imgName = [imgURL lastPathComponent];
        
        NSData* imgData = nil;
        
        if([[NSFileManager defaultManager] fileExistsAtPath:[directory stringByAppendingPathComponent:imgName] isDirectory:nil])
             imgData = [NSData dataWithContentsOfFile:[directory stringByAppendingPathComponent:imgName]];
         else if([Helpers NetworkIsReacheable]) 
         { 
             imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgURL]];
             [imgData writeToFile:[directory stringByAppendingPathComponent:imgName] atomically:YES];
         }
        
        if(imgData != nil)
        [button setImage:[UIImage imageWithData:imgData] forState:UIControlStateNormal];
    }
    
    if(!parentViewController.videoButton.enabled)
    {
        UIImageView* playImg = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playbutton-potratit.png"]] autorelease];
        
        playImg.frame = CGRectMake((button.frame.size.width / 2) - 22.5, (button.frame.size.height / 2) - 22, 45, 44);
        [button addSubview:playImg];
    }
    
    [indicator removeFromSuperview];
    [pool release];
}


-(void)gridObjectSelected:(UIButton*)sender{
    
    [parentViewController itemSelectedWithObject:[objects objectAtIndex:sender.tag]];
}
@end
