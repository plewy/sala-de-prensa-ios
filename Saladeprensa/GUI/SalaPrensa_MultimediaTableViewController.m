//
//  SalaPrensa_MultimediaTableViewController.m
//  Saladeprensa
//
//  Created by Lionel Ariel Fiszman on 19/03/12.
//  Copyright (c) 2012 Plewy. All rights reserved.
//

#import "SalaPrensa_MultimediaTableViewController.h"
#import "SalaPrensa_MultimediaTableViewCell.h"
#import "helpers.h"
#import "MBProgressHUD.h"

@interface SalaPrensa_MultimediaTableViewController ()

@end
MBProgressHUD* HUD = nil;
@implementation SalaPrensa_MultimediaTableViewController
@synthesize objectsArray = _objectsArray;
@synthesize urlPhotoString;
@synthesize urlVideoString;
@synthesize feedTable;
@synthesize videoButton;
@synthesize photoButton;
@synthesize delegate;


- (void)viewDidLoad
{
    photoButton.enabled = NO;
    videoButton.enabled = YES;
    
    urlVideoString = @"http://plewy.com/demos/readersoft/videos.xml";
    urlPhotoString =@"http://sala-prensa.com/telefonica/contents/rss/20-novedades.xml/zone:1/lang:es";
    HUD = nil;
    
   
    [self performSelectorInBackground:@selector(loadFeeds) withObject:nil];
    
     [super viewDidLoad];
}


-(void)loadFeeds{
    
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    
    NSString* urlString;
    
    if(!photoButton.enabled)
        urlString = urlPhotoString;
    else 
        urlString = urlVideoString;
    
    if(HUD == nil){  
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.labelText = @"Cargando...";
        [self.view addSubview:HUD];}
    [HUD show:YES];
    
     
    if(self.objectsArray != nil)
        [self.objectsArray release];
    
    self.objectsArray = [Helpers retrieveXMLfromURL:urlString:@"item":@"home" ];
    
    [self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];
    
    [pool release];
}

-(void)reloadTable{
    feedTable.delegate = self;
    feedTable.dataSource = self;
    [feedTable reloadData];
    [HUD hide:YES];
        // [HUD removeFromSuperview];
        //[HUD release];
}

- (void)viewDidUnload
{
    [self setPhotoButton:nil];
    [self setVideoButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   if([self.objectsArray count] % 3 == 0) 
       return [self.objectsArray count] / 3;
   else 
       return ([self.objectsArray count] / 3) + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    SalaPrensa_MultimediaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if(cell == nil)
        cell = [[SalaPrensa_MultimediaTableViewCell alloc] init];

    if([self.objectsArray count] <= 3)
    {
        cell.objects = self.objectsArray;
    }
    else
    {
        
        NSRange range = NSMakeRange(indexPath.row * 3, 3);
        
        
        if(range.length + range.location > [self.objectsArray count])
            range.length = [self.objectsArray count] - range.location;
        
        cell.objects = [self.objectsArray subarrayWithRange:range];
    }
    cell.parentViewController = self;
    
    [cell loadCell];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
 
}

-(void)itemSelectedWithObject:(NSDictionary*)obj{

    int i = 0;

    while(i < [self.objectsArray count] &&  ![[self.objectsArray objectAtIndex:i] isEqual:obj]) 
        i++;
    
    
    if(!photoButton.enabled)
        [delegate PushMultimediaWithObjectArray:self.objectsArray andIndex:i];
    else if([Helpers NetworkIsReacheable]){
     
        for(NSString* url in [obj objectForKey:@"media"])
        {
            if([url rangeOfString:@"youtube"].length > 0)
            {
                [self openVideoWithURL:url];
                break;
            }
        }
        
    }
}

-(void)openVideoWithURL:(NSString*) url{
    NSString *htmlString =  @"<html><head><style type=\"text/css\"> body {background-color: transparent; color: white;}</style></head><body style=\"margin:0\"><embed id=\"yt\" src=\"[URL]&autoplay=1\" type=\"application/x-shockwave-flash\"width=\"[W]\" height=\"[H]\"></embed></body></html>"; 
	htmlString = [htmlString stringByReplacingOccurrencesOfString:@"[URL]" withString:url];
    
    
    UIWebView* videoView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 10, 300, 300)];  
    videoView.delegate = self;
    videoView.hidden = YES;
    [self.view addSubview:videoView];
    [videoView loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"http://www.sala-prensa.com"]];
    [videoView release];
}

- (void)webViewDidFinishLoad:(UIWebView *)_webView {
    UIButton *b = [self findButtonInView:_webView];
    [b sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (UIButton *)findButtonInView:(UIView *)view {
    UIButton *button = nil;
    
    if ([view isMemberOfClass:[UIButton class]]) {
        return (UIButton *)view;
    }
    
    if (view.subviews && [view.subviews count] > 0) {
        for (UIView *subview in view.subviews) {
            button = [self findButtonInView:subview];
            if (button) return button;
        }
    }
    return button;
}


- (void)dealloc {
    [feedTable release];
    [photoButton release];
    [videoButton release];
    [super dealloc];
}
- (IBAction)photoSelected:(id)sender {
    
    photoButton.enabled = NO;
    videoButton.enabled = YES;
    
    [self performSelectorInBackground:@selector(loadFeeds) withObject:nil];
    
}

- (IBAction)videoSelected:(id)sender {

    photoButton.enabled = YES;
    videoButton.enabled = NO;
    [self performSelectorInBackground:@selector(loadFeeds) withObject:nil];
    
}
@end
