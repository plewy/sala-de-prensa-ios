//  Created by Lionel Ariel Fiszman on 14/10/11.
//  Copyright 2011 Plewy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TBXML.h"
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"


@interface Helpers : NSObject <ASIHTTPRequestDelegate,ASIProgressDelegate>

+ (UIColor *)colorForHex:(NSString *)hexColor;
+ (UIImage *)resizeImage:(UIImage *)image width:(int)width height:(int)height;
+ (UIImage *)reflectedImage:(UIImageView *)fromImage withHeight:(NSUInteger)height;
+ (NSArray*)retrieveXMLfromFile:(NSString*)filename:(NSString*)rootElementName;
+ (NSArray*)retrieveXMLfromURL:(NSString*)URLString:(NSString*)rootElementName:(NSString*)fileName;
+ (float) groupedCellMarginWithTableWidth:(float)tableViewWidth;
+ (BOOL)NSStringIsValidEmail:(NSString *)checkString;
+ (BOOL)ReacheableByUrl:(NSString*)url;
+ (BOOL)NetworkIsReacheable;
+ (BOOL)ReacheablebyWifi;
+ (BOOL)CreateDirectory:(NSString*)directory;
+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2;


static NSInteger numericCompare(id first, id second, void* context);

@end
