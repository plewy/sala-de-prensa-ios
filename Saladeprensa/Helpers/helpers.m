#import "Helpers.h"
#import "Reachability.h"


@implementation Helpers

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+ (UIColor *) colorForHex:(NSString *)hexColor {
	
    hexColor = [[hexColor stringByTrimmingCharactersInSet:
				 [NSCharacterSet whitespaceAndNewlineCharacterSet]
                 ] uppercaseString];  
    
    // String should be 6 or 7 characters if it includes '#'  
    if ([hexColor length] == 6) 
		return [UIColor blackColor];  
    
    

    // strip # if it appears  
    if ([hexColor hasPrefix:@"#"]) 
		hexColor = [hexColor substringFromIndex:1];  
    
    // if the value isn't 6 characters at this point return 
    // the color black	
    if ([hexColor length] != 6) 
		return [UIColor blackColor];  
    
    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2; 
    
    NSString *rString = [hexColor substringWithRange:range];  
    
    range.location = 2;  
    NSString *gString = [hexColor substringWithRange:range];  
    
    range.location = 4;  
    NSString *bString = [hexColor substringWithRange:range];  
    
    // Scan values  
     unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  
    
    return [UIColor colorWithRed:((float) r / 255.0f)  
                           green:((float) g / 255.0f)  
                            blue:((float) b / 255.0f)  
                           alpha:1.0f];  
    
}

+ (UIImage *)resizeImage:(UIImage *)image width:(int)width height:(int)height {
	
	CGImageRef imageRef = [image CGImage];
	CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
	
	//if (alphaInfo == kCGImageAlphaNone)
    alphaInfo = kCGImageAlphaNoneSkipLast;
	
	CGContextRef bitmap = CGBitmapContextCreate(NULL, width, height, CGImageGetBitsPerComponent(imageRef), 4 * width, CGImageGetColorSpace(imageRef), alphaInfo);
	CGContextDrawImage(bitmap, CGRectMake(0, 0, width, height), imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(bitmap);
	UIImage *result = [UIImage imageWithCGImage:ref];
	
	CGContextRelease(bitmap);
	CGImageRelease(ref);
	
	return result;	
}


#pragma mark - Image Reflection

CGImageRef CreateGradientImage(int pixelsWide, int pixelsHigh)
{
	CGImageRef theCGImage = NULL;
    
	// gradient is always black-white and the mask must be in the gray colorspace
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
	
	// create the bitmap context
	CGContextRef gradientBitmapContext = CGBitmapContextCreate(NULL, pixelsWide, pixelsHigh,
															   8, 0, colorSpace, kCGImageAlphaNone);
	
	// define the start and end grayscale values (with the alpha, even though
	// our bitmap context doesn't support alpha the gradient requires it)
	CGFloat colors[] = {0.0, 1.0, 1.0, 1.0};
	
	// create the CGGradient and then release the gray color space
	CGGradientRef grayScaleGradient = CGGradientCreateWithColorComponents(colorSpace, colors, NULL, 2);
	CGColorSpaceRelease(colorSpace);
	
	// create the start and end points for the gradient vector (straight down)
	CGPoint gradientStartPoint = CGPointZero;
	CGPoint gradientEndPoint = CGPointMake(0, pixelsHigh);
	
	// draw the gradient into the gray bitmap context
	CGContextDrawLinearGradient(gradientBitmapContext, grayScaleGradient, gradientStartPoint,
								gradientEndPoint, kCGGradientDrawsAfterEndLocation);
	CGGradientRelease(grayScaleGradient);
	
	// convert the context into a CGImageRef and release the context
	theCGImage = CGBitmapContextCreateImage(gradientBitmapContext);
	CGContextRelease(gradientBitmapContext);
	
	// return the imageref containing the gradient
    return theCGImage;
}

CGContextRef MyCreateBitmapContext(int pixelsWide, int pixelsHigh)
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	
	// create the bitmap context
	CGContextRef bitmapContext = CGBitmapContextCreate (NULL, pixelsWide, pixelsHigh, 8,
														0, colorSpace,
														// this will give us an optimal BGRA format for the device:
														(kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst));
	CGColorSpaceRelease(colorSpace);
    
    return bitmapContext;
}

+ (UIImage *)reflectedImage:(UIImageView *)fromImage withHeight:(NSUInteger)height
{
    if(height == 0)
		return nil;
    
	// create a bitmap graphics context the size of the image
	CGContextRef mainViewContentContext = MyCreateBitmapContext(fromImage.bounds.size.width, height);
	
	// create a 2 bit CGImage containing a gradient that will be used for masking the 
	// main view content to create the 'fade' of the reflection.  The CGImageCreateWithMask
	// function will stretch the bitmap image as required, so we can create a 1 pixel wide gradient
	CGImageRef gradientMaskImage = CreateGradientImage(1, height);
	
	// create an image by masking the bitmap of the mainView content with the gradient view
	// then release the  pre-masked content bitmap and the gradient bitmap
	CGContextClipToMask(mainViewContentContext, CGRectMake(0.0, 0.0, fromImage.bounds.size.width, height), gradientMaskImage);
	CGImageRelease(gradientMaskImage);
	
	// In order to grab the part of the image that we want to render, we move the context origin to the
	// height of the image that we want to capture, then we flip the context so that the image draws upside down.
	CGContextTranslateCTM(mainViewContentContext, 0.0, height);
	CGContextScaleCTM(mainViewContentContext, 1.0, -1.0);
	
	// draw the image into the bitmap context
	CGContextDrawImage(mainViewContentContext, fromImage.bounds, fromImage.image.CGImage);
	
	// create CGImageRef of the main view bitmap content, and then release that bitmap context
	CGImageRef reflectionImage = CGBitmapContextCreateImage(mainViewContentContext);
	CGContextRelease(mainViewContentContext);
	
	// convert the finished reflection image to a UIImage 
	UIImage *theImage = [UIImage imageWithCGImage:reflectionImage];
	
	// image is retained by the property setting above, so we can release the original
	CGImageRelease(reflectionImage);
	
	return theImage;
}


+(NSArray*)retrieveXMLfromFile:(NSString*)filename:(NSString*)rootElementName{
    
    NSMutableArray* array = [[[NSMutableArray alloc] init] retain];
    
    TBXML* tbxml = [[TBXML tbxmlWithXMLFile:filename] retain];
    
    if(tbxml.rootXMLElement)
    {
        TBXMLElement* item = [TBXML childElementNamed:rootElementName parentElement:tbxml.rootXMLElement];
        
        while(item != nil){
            
            NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
            
            TBXMLElement * child = item->firstChild;
            
            while (child) {
                [dict setObject:[NSString stringWithCString:child->text encoding:NSUTF8StringEncoding]   forKey:[NSString stringWithCString:child->name encoding:NSUTF8StringEncoding]];
                child = child->nextSibling;
            }
            
           // [dict setObject: forKey:];
            
            
            [array addObject:dict];
            item = [TBXML nextSiblingNamed:rootElementName searchFromElement:item];
        }
            
    }
    
    
    return array;
}

+ (NSArray*)retrieveXMLfromURL:(NSString*)URLString:(NSString*)rootElementName:(NSString*)fileName{
    
    NSMutableArray* array = [[[NSMutableArray alloc] init] retain];
    
    TBXML* tbxml;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file = [documentsDirectory stringByAppendingPathComponent:fileName];

    
    if([self NetworkIsReacheable]){
    
        NSString* string = [[NSString alloc] initWithContentsOfURL:[NSURL URLWithString:URLString] encoding:NSUTF8StringEncoding error:nil];
        [string writeToFile:file atomically:YES encoding:NSUTF8StringEncoding error:nil];

        tbxml = [[TBXML tbxmlWithXMLString:string] retain]; 
    }else
    {
        if([[NSFileManager defaultManager] fileExistsAtPath:file])
        {
            NSString* string = [[NSString alloc] initWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil];
            [string writeToFile:file atomically:YES encoding:NSUTF8StringEncoding error:nil];
            
            tbxml = [[TBXML tbxmlWithXMLString:string] retain]; 
            
        }else
        {return nil;}
    }
    
    
    if(tbxml.rootXMLElement)
    {
        
        TBXMLElement* channelItem =[TBXML childElementNamed:@"channel" parentElement:tbxml.rootXMLElement];
        
        
        TBXMLElement* item = [TBXML childElementNamed:rootElementName parentElement:channelItem];
        
        while(item != nil){
            
            NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
            
            TBXMLElement * child = item->firstChild;
            
            while (child) {
                
                if([[NSString stringWithCString:child->name	 encoding:NSUTF8StringEncoding] isEqualToString:@"media:group"])
                {
                    
                    NSMutableArray* mmArray = [[NSMutableArray alloc] init];
                    
                    TBXMLElement* mediaChild = child->firstChild;
                    
                    while (mediaChild) {
                         
                        [mmArray addObject:[NSString stringWithCString:mediaChild->firstAttribute->value encoding:NSUTF8StringEncoding]];
                        
                       mediaChild = mediaChild->nextSibling;
                    }
                    
                    [dict setObject:mmArray forKey:@"media"];
                }
                else
                [dict setObject:[NSString stringWithCString:child->text encoding:NSUTF8StringEncoding]   forKey:[NSString stringWithCString:child->name encoding:NSUTF8StringEncoding]];
               
                child = child->nextSibling;
            }
            
            // [dict setObject: forKey:];
            
            
            [array addObject:dict];
            item = [TBXML nextSiblingNamed:rootElementName searchFromElement:item];
        }
        
    }
    
    
    return array;
}

static NSInteger numericCompare(id first, id second, void* context)
{
    return (NSInteger)[(NSString*)[first distance] compare:[second distance] options:NSCaseInsensitiveSearch | NSNumericSearch];
}

+ (float) groupedCellMarginWithTableWidth:(float)tableViewWidth
{
    float marginWidth;
    if(tableViewWidth > 20)
    {
        if(tableViewWidth < 400)
        {
            marginWidth = 10;
        }
        else
        {
            marginWidth = MAX(31, MIN(45, tableViewWidth*0.06));
        }
    }
    else
    {
        marginWidth = tableViewWidth - 10;
    }
    return marginWidth;
}



+ (BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; 
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (BOOL)ReacheableByUrl:(NSString*)url{
    
    Reachability* hostReach;
    hostReach = [[Reachability reachabilityWithHostName:url] retain];
	//[hostReach startNotifier];
	
	NetworkStatus netStatus = [hostReach currentReachabilityStatus];
    
    return YES;
}

+ (BOOL)NetworkIsReacheable{
    
    Reachability* hostReach;
    hostReach = [[Reachability reachabilityForInternetConnection] retain];
	//[hostReach startNotifier];
	
	NetworkStatus netStatus = [hostReach currentReachabilityStatus];
    
    if(netStatus == NotReachable)
        return NO;
    else
        return YES;
    
}

+ (BOOL)ReacheablebyWifi{
    
    Reachability* hostReach;
    hostReach = [[Reachability reachabilityForInternetConnection] retain];
	//[hostReach startNotifier];
	
	NetworkStatus netStatus = [hostReach currentReachabilityStatus];
    
    if(netStatus == ReachableViaWiFi)
        return YES;
    else
        return NO;
}

+(BOOL)CreateDirectory:(NSString*)directory{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    directory = [documentsDirectory stringByAppendingPathComponent:directory];
    
    
    if([[NSFileManager defaultManager] fileExistsAtPath:directory isDirectory:nil])
    { 
        return YES;
    }
    else
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:nil];
        return NO;
    }
    
}


+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 {  
    
    
    UIGraphicsBeginImageContext(image1.size);  
    
        // Draw image1  
    [image1 drawInRect:CGRectMake(0, 0, image1.size.width, image1.size.height)];  
    
        // Draw image2  
    [image2 drawInRect:CGRectMake(image1.size.width - image2.size.width, 0, image2.size.width, image2.size.height)];  
    
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();  
    
    UIGraphicsEndImageContext();  
    
    return resultingImage;  
}  


+(void)DownloadFileFromURL:(NSURL*)url inPath:(NSString*)path showProgressInview:(UIView*)view{
    
    
    
}

@end
